import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-some2-without-child',
  templateUrl: './some2-without-child.component.html',
  styleUrls: ['./some2-without-child.component.css']
})
export class Some2WithoutChildComponent implements OnInit, OnChanges {

  @Input('objData') objData;

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges() {
    console.log('Some2WithoutChildComponent :: ngOnChanges()');
  }

  changeMutableData() {
    this.objData.name = 'MutateD! by Some2 component!';
  }

}

