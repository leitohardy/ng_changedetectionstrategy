import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ChildViewComponent } from './child-view/child-view.component';
import { SubChildComponent } from './child-view/sub-child/sub-child.component';
import { Some2WithoutChildComponent } from './some2-without-child/some2-without-child.component';

@NgModule({
  declarations: [
    AppComponent,
    ChildViewComponent,
    SubChildComponent,
    Some2WithoutChildComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
