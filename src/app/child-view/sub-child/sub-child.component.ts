import { Component, OnInit, Input, OnChanges, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-sub-child',
  templateUrl: './sub-child.component.html',
  styleUrls: ['./sub-child.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SubChildComponent implements OnInit, OnChanges{

  @Input('objData') objData;

  titleText = {
    str: 'www'
  };
  
  constructor() { }

  ngOnInit() {
  }

  ngOnChanges() {
    console.log('Sub Child ::: ngOnChanges()');
  }

  immutate() {
    this.objData = {name: 'mutateBysub', number: 777};
    this.titleText.str = 'changed string'; // mutalbe!
  }

}
