import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-child-view',
  templateUrl: './child-view.component.html',
  styleUrls: ['./child-view.component.css']
})
export class ChildViewComponent implements OnInit, OnChanges {

  @Input('objData') objData;

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges() {
    console.log('--Child-- component :: ngOnChanges()');
  }

  changeMutableData() {
    this.objData.name = 'Mutate by Child component!';
  }

}
