import { Component, OnChanges } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnChanges {
  title = 'changeDetection';

  inputData = {
    name: 'Mark',
    number: 14
  };

  objData = {
    name: 'Mark',
    number: 14
  }

  // mutable and immutable
  changeObj_mutable() {
    this.objData.name = this.inputData.name;
    this.objData.number = this.inputData.number;
  }

  changeObj_immutable() {
    this.objData = {
      name: this.inputData.name,
      number: this.inputData.number
    }
  }

  ngOnChanges() {
    console.log('-Root- :: ngOnChanges()!');
  }

}
